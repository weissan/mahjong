"use strict"
const userModel = require('../model/user.js');

var userDao = module.exports;

userDao.getUser = async function(arg) {
  var res;
  var err,doc = await userModel.findOne({username:arg.username});

  if (err) {
  } else if (doc) {
  } else {
    err = "user doesn't exist"
    doc = undefined
  }

  return err,doc
}

userDao.createUser = async function(arg) {
  var res;
  var err,doc = await userModel.findOne({username:arg.username});

  if (err) {
  } else if (doc) {
    err = arg.username + " already exists"
    doc = undefined
  } else {
    err,doc = await userModel.create({username:arg.username,password:arg.password});
  }

  return err,doc
}
