"use strict"
const mongoose = require("mongoose");
const config = require("../config.js");

mongoose.Promise = global.Promise;
// Connect to DB
mongoose.connect(config.MONGO_URL);

module.exports = mongoose;
