"use strict"
const db = require('../dao/db.js');

var UserSchema   = new db.Schema({
    username: { type: String, required: true, unique: true, dropDups: true },
    password: { type: String, required: true }
});

module.exports = db.model('user', UserSchema);
