"use strict"
const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const jwt = require("koa-jwt");
const jsonwebtoken = require("jsonwebtoken");
const config = require("./config.js");
const userDao = require('./dao/user.js');

var app = new Koa();
var router = new Router();

app.use(bodyParser({
  onerror: function (err, ctx) {
    ctx.throw('body parse error', 422);
  }
}));

app.use(jwt({ secret: config.JWT_SECRET, key: 'jwtdata' }).unless({ path: [/^\/sign/] }));

router.post('/signin',async (ctx, next) => {
  var err,doc = await userDao.getUser(ctx.request.body);

  if (err) {
  } else if (ctx.request.body.password == doc.password) {
    var token = jsonwebtoken.sign({
      username: ctx.request.body.username
      }, config.JWT_SECRET);
    doc = token;
  }
  else {
    err = "Password incorrect";
    doc = undefined
  }

  ctx.body = {err:err, doc:doc};

  await next();
});

router.post('/signup',async (ctx, next) => {
  var err,doc = await userDao.createUser(ctx.request.body);

  ctx.body = {err:err, doc:doc};

  await next();
});

router.get('/verify',async (ctx, next) => {
  ctx.body = ctx.state.jwtdata;

  await next();
});


app.use(router.routes());
app.use(router.allowedMethods());

app.use(async (ctx, next) => {
  ctx.response.set('Access-Control-Allow-Origin', '*');
  ctx.response.set('Access-Control-Allow-Methods', 'GET, POST');
  ctx.response.set('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
  await next();
});

app.listen(config.PORT);
