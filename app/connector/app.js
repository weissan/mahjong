"use strict"
const Server = require('socket.io')
const Redis = require('ioredis');
const adapter = require('socket.io-redis');
const config = require("./config.js");

var server = new Server();
server.attach(process.env.PORT_CONNECTOR || config.PORT_CONNECTOR)

var pub = new Redis.Cluster(config.REDIS_CLUSTER);
var sub = new Redis.Cluster(config.REDIS_CLUSTER);
var redis = new Redis.Cluster(config.REDIS_CLUSTER);
server.adapter(adapter({ pubClient: pub, subClient: sub }));

// Chatroom

var numUsers = 0;

server.on('connection', function (socket) {
  var addedUser = false;
  console.log('Remote ip is:'+socket.handshake.address);
  if (socket.handshake.address != '::ffff:' + (process.env.IP_REVERSEPROXY || config.IP_REVERSEPROXY)) {
    socket.disconnect(true);
  }

  // when the client emits 'new message', this listens and executes
  socket.on('new message', function (data) {
    // we tell the client to execute 'new message'
    socket.broadcast.emit('new message', {
      username: socket.username,
      message: data
    });
  });

  // when the client emits 'add user', this listens and executes
  socket.on('add user', async (username) => {
    if (addedUser) return;

    // we store the username in the socket session for this client
    socket.username = username;
    //++numUsers;
    var err, reply = await redis.incr('numUsers')
    numUsers = reply;

    addedUser = true;
    socket.emit('login', {
      numUsers: numUsers
    });

    // echo globally (all clients) that a person has connected
    socket.broadcast.emit('user joined', {
      username: socket.username,
      numUsers: numUsers
    });
  });

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', function () {
    socket.broadcast.emit('typing', {
      username: socket.username
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', function () {
    socket.broadcast.emit('stop typing', {
      username: socket.username
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', async () => {
    if (addedUser) {
      //--numUsers;
      var err, reply = await redis.decr('numUsers')
      numUsers = reply;

      // echo globally that this client has left
      socket.broadcast.emit('user left', {
        username: socket.username,
        numUsers: numUsers
      });
    }
  });
});
