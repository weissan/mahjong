var config = {
  PORT_CONNECTOR: '3000',
  IP_REVERSEPROXY: '127.0.0.1',
  MONGO_URL: 'db.wqueue.com:27017/mahjong',
  JWT_SECRET: 'secret',
  REDIS_CLUSTER: [{
		host: '23.106.141.95',
		port: 6379,
    password: 'wqueue',
	}, {
		host: '104.194.74.46',
		port: 6379,
    password: 'wqueue',
	}, {
		host: '23.105.210.155',
		port: 6379,
    password: 'wqueue',
	}, {
		host: '23.106.128.102',
		port: 6379,
    password: 'wqueue',
	}, {
		host: '23.106.158.189',
		port: 6379,
    password: 'wqueue',
	}, {
		host: '45.78.12.129',
		port: 6379,
    password: 'wqueue',
	}],
};

module.exports = config;
